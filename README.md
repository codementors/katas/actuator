# Spring Security & Actuator Kata

Diese Kata dient als Übung bzw. Vorlage zur Installation und Konfiguration von
Spring Web Security mit Actuator

## Hintergrund

Die Actuator Endpoints von Spring Security werden häufig auf einen Port mit der
Anwendung gelegt. Das führt dazu, dass die Informationen und Funktionen von
Actuator öffentlich im Web verfügbar sind.

## Voraussetzungen

Diese Kata verwendet Entwickler SSL Zertifikate. Um diese lokal zu installieren,
muss `mkcert` installiert werden.

```shell
brew install mkcert
mkcert --install # run as root
```

Um das Zertifikat zu erzeugen:

```shell
cd tools
./create_certifcate.sh
```

## Szenario

Diese Kata enthält eine Mini-Anwendung mit einem geschützen Bereich und einer
Startseite (
siehe [Spring Security Guide](https://spring.io/guides/gs/securing-web)).

Diese Anwendung soll im Cluster laufen und braucht aus diesem Grund einen
Healthcheck. Die Metriken des Services sollen mit Prometheus eingesammelt
werden.

Der Health Check soll nicht auf dem gleichen Port wie die Anwendung laufen.
Die Prometheus Metriken sollen nicht auf dem gleichen Port wie die Anwendung
angeboten werden.

## Referenzen

- [Spring Security Reference](https://docs.spring.io/spring-security/reference/index.html)
- [Spring Security Guide](https://spring.io/guides/gs/securing-web)
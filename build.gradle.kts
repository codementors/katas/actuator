import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  id("org.springframework.boot") version "3.2.2"
  id("io.spring.dependency-management") version "1.1.4"
  kotlin("jvm") version "1.9.22"
  kotlin("plugin.spring") version "1.9.22"
}

group = "de.codementors.kata"
version = "0.0.1-SNAPSHOT"

java {
  sourceCompatibility = JavaVersion.VERSION_17
}

repositories {
  mavenCentral()
}

dependencies {

  implementation("org.springframework.boot", "spring-boot-starter-actuator")
  implementation("org.springframework.boot", "spring-boot-starter-thymeleaf")
  implementation("org.thymeleaf.extras", "thymeleaf-extras-springsecurity6")
  implementation("org.springframework.boot", "spring-boot-starter-web")
  implementation("org.springframework.boot", "spring-boot-starter-security")
  implementation("com.fasterxml.jackson.module", "jackson-module-kotlin")
  implementation("org.jetbrains.kotlin", "kotlin-reflect")
  implementation("io.micrometer", "micrometer-registry-prometheus")

  testImplementation("org.springframework.boot", "spring-boot-starter-test")
  testImplementation("io.rest-assured", "rest-assured", "5.4.0")
  testImplementation("io.rest-assured", "kotlin-extensions", "5.4.0")
}

tasks.withType<KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs += "-Xjsr305=strict"
    jvmTarget = "17"
  }
}

tasks.withType<Test> {
  useJUnitPlatform()
}

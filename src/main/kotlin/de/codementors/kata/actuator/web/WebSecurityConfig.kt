package de.codementors.kata.actuator.web

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.invoke
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.provisioning.InMemoryUserDetailsManager
import org.springframework.security.web.SecurityFilterChain

@Configuration
@EnableWebSecurity
class WebSecurityConfig {

  @Bean
  fun securityFilterChain(http: HttpSecurity): SecurityFilterChain {
    http {
      authorizeHttpRequests {
        authorize("/", permitAll)
        authorize("/actuator/health/**", permitAll)
        authorize("/actuator/health", permitAll)
        authorize("/actuator/prometheus", hasRole("MONITORING"))
        authorize("/home", permitAll)
        authorize("/hello", hasRole("USER"))
        authorize(anyRequest, authenticated)
      }
      formLogin {
        loginPage = "/login"
        permitAll()
      }
      logout {
        permitAll()
      }
      httpBasic { }
    }

    return http.build()
  }

  @Bean
  fun userDetailsService(): UserDetailsService {
    val monitor = User.withDefaultPasswordEncoder()
      .username("monitor")
      .password("pass")
      .roles("MONITORING")
      .build()
    val user = User.withDefaultPasswordEncoder()
      .username("user")
      .password("pass")
      .roles("USER")
      .build()

    return InMemoryUserDetailsManager(monitor, user)
  }
}

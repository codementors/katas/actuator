package de.codementors.kata.actuator

import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@DisplayName("Test Spring is configured properly")
@SpringBootTest
class ActuatorApplicationTests {

  @Test
  fun contextLoads() {
  }
}

package de.codementors.kata.actuator

import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.boot.test.web.server.LocalServerPort

@DisplayName("Web Application Security Tests")
@SpringBootTest(
  webEnvironment = WebEnvironment.RANDOM_PORT,
  properties = ["server.ssl.enabled=false"],
)
class ApplicationEndpointTests {

  @LocalServerPort
  private var port: Int = 0

  @Test
  fun `call public start page should return 200`() {
    Given {
      port(port)
    } When {
      log().all()
      get("/")
    } Then {
      log().all()
      statusCode(200)
    }
  }
}

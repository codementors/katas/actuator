package de.codementors.kata.actuator

import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalManagementPort
import org.springframework.boot.test.web.server.LocalServerPort

@DisplayName("Actuator Management Security Tests")
@SpringBootTest(
  webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
  properties = [ "server.ssl.enabled=false" ],
)
class ManagementEndpointTests {

  @LocalServerPort
  val localServerPort: Int = 0

  @LocalManagementPort
  val localManagementPort: Int = 0

  @Test
  fun `given management port when request then health endpoint is accessible`() {
    Given {
      port(localManagementPort)
    } When {
      get("/actuator/health")
    } Then {
      statusCode(200)
    }
  }

  @Test
  fun `given no auth when request prometheus then access denied`() {
    Given {
      port(localManagementPort)
    } When {
      get("/actuator/prometheus")
    } Then {
      statusCode(401)
    }
  }

  @Test
  fun `given basic auth when request prometheus then access allowed`() {
    Given {
      port(localManagementPort)
      auth().basic("monitor", "pass")
    } When {
      get("/actuator/prometheus")
    } Then {
      statusCode(200)
    }
  }

  @Test
  fun `given server port when request then health endpoint then is not accessible`() {
    Given {
      port(localServerPort)
    } When {
      get("/actuator/health")
    } Then {
      statusCode(401)
    }
  }

  @Test
  fun `given server port when request then prometheus endpoint then is not accessible`() {
    Given {
      port(localServerPort)
    } When {
      get("/actuator/prometheus")
    } Then {
      statusCode(401)
    }
  }
}
